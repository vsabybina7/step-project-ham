
//////////start:container-our-services/////////
$(document).ready(function($) {
    $('.content-text').hide();
    $('#content1').show();
    $('.menu-item').click(function() {
        $('.menu-item-active').removeClass('menu-item-active');
        $(this).addClass('menu-item-active');
        $('.content-text')
            .hide()
            .filter('[id="' + $(this).data('index') + '"]')
            .show();

    });
//////////end:container-our-services//////////

/////////start: container-our-work ///////////

    $('.our-img-hover').hide();
    $('.our-img-hover').show();

    $('.our-work-menu-item').click(function() {

        $('.our-work-menu-item-active').removeClass('our-work-menu-item-active');
        $(this).addClass('our-work-menu-item-active');

        const currentCategory = $(this).data('category');

        $('.our-img-hover-active').removeClass('div>our-img-hover-active');


        $('.category').each(function (index, item) {

            if ($(item).data('category') === currentCategory
                || currentCategory === undefined
            ) {
                $(item).show();
                $('.our-img-hover-active').addClass('our-img-hover');
                $('.our-img-hover-active').removeClass('our-img-hover-active').hide();

            } else {
                $(item).hide();
                $('.our-img-hover-active').css('opacity', '0');
                $('.our-img-hover-active').addClass('our-img-hover');
                $('.our-img-hover').show();
            }
        });
    });
//нажатие кнопки load
    $('.block-img-load').hide();

    $('.our-work-button-load').click(function () {
        $('.our-img-hover-active')
            .hide()
            .addClass('our-img-hover')
            .show();
        if($('.block-img-load').hide()) {
            $('.block-img-load').slideDown();
            $('.our-work-button-load').remove();

        }else {
            $('.block-img-load').hide();
        }
    });
//////////end:container-our-work ///////////

//////////start:container-for-Ham///////////

    //отображение соответствующих фото и контента
    $('.block-worker-hide').hide();
    $('.small-photo').click(slider);

    const arrow1 = document.getElementById('arrow1');
    const arrow2 =  document.getElementById('arrow2');

  // slider for click arrows
    function slider() {
        $(this).removeClass('small-photo-active');
        $(this).addClass('small-photo-active');

        $('.photo-active').removeClass('photo-active');
        $(this).children().addClass('photo-active');

        $('.top-active').removeClass('top-active');
        $(this).addClass('top-active');

        const currentCategory = $(this).data('name');

        $('.block-information-of-worker').each(function (index, item) {
            if ($(item).data('name') === currentCategory) {
                $(item).show();
            }else {
                $(item).hide();
            }
        });
    }

    const photos = document.querySelectorAll('.small-photo');
    const photosArray = Array.prototype.slice.call(photos);

    arrow1.addEventListener('click', function () {
        for(let i =0,flag=true;flag;i++) {
            if (photosArray[i].classList.contains("top-active")) {
                let arrowSlider = slider.bind(photosArray
                    [i ? i - 1 : photosArray.length - 1]);
                arrowSlider();
                flag = false;
            }
        }

    });
    arrow2.addEventListener('click', function () {
        for(let j =0,flag=true;flag;j++) {
            if (photosArray[j].classList.contains("top-active")) {
                let arrowSlider2 = slider.bind(photosArray [j===photosArray.length-1 ? 0 : (j+1)]);
                arrowSlider2();
                flag = false;
            }
        }
    });
//////////end:container-for-Ham////////////

});